import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        // primary: colors.purple,
        // secondary: colors.grey.darken1,
        // accent: colors.shades.black,
        // error: colors.red.accent3,

        white2: "#faebd7",
        kuk: "#4d5bf3",
        yashil: "#90f6e3",
        yashil2: "#7cddd8",
        navbg: "#0061F6",
      },
      dark: {
        // primary: colors.blue.lighten3,
        white2: "#946930",
        kuk: "#4d5bf3",
        yashil: "#2b7265",
        yashil2: "#F9AAF6",
        navbg: "#FFC4B4",
      },
    },
  },
});
