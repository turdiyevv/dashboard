import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'

import './assets/scss/_spets.scss'
import './assets/scss/_manage.scss'
import './assets/scss/_custom.scss'

import store from './store';
import axios from "axios";

Vue.config.productionTip = false

new Vue({
  router,
  axios,
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app')
