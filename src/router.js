import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: () => import("@/pages/DashboardPage"),
      name: "DashboardPage",
    },
    {
      path: "/posts",
      component: () => import("@/pages/postsPage"),
      name: "postsPage",
    },
    {
      path: "/main",
      component: () => import("@/pages/mainPage"),
      name: "mainPage",
    },
    {
      path: "/table",
      component: () => import("@/pages/tablePage"),
      name: "tablePage",
    },
    {
      path: "/cards",
      component: () => import("@/pages/CardPage"),
      name: "CardPage",
    },
    {
      path: "/tester",
      component: () => import("@/pages/tester"),
      name: "tester",
    },
    {
      path: "/actions",
      component: () => import("@/pages/Actions"),
      name: "Actions",
    },
    {
      path: "/extentions",
      component: () => import("@/pages/ExtentionsPage"),
      name: "ExtentionsPage",
    },
    {
      path: "/users",
      component: () => import("@/pages/usersPage"),
      name: "usersPage",
    },
    {
      path: "/settings",
      component: () => import("@/pages/settingsPage"),
      name: "settingsPage",
    },
  ],
});



export default router
