import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pageModeVar: 'ligth',
  },
  mutations: {
    pageMode(state, val) {
      state.pageModeVar = val
    }
  },
  getters: {
    getPageMode(state) {
      return state.pageModeVar
    }
  }
});
